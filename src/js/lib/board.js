define([], function () {
    "use strict";
    function Board() {
        this.taken = { 8: false, 1: false, 6: false,
                       3: false, 5: false, 7: false,
                       4: false, 9: false, 2: false };
        this.clone = function () {
            var k, clone = new Board();
            for (k = 1; k < 10; k += 1) {
                clone.taken[k] = this.taken[k];
            }
            return clone;
        };

        this.history = [];
        this.pairs = {};

        this.whos_turn = function () {
            return (this.get_open_moves().length % 2 === 1 ? "X" : "O");
        };


        this.is_won = function (for_piece) {
            var rows, i, j, row_won;
            rows = [
                [8, 1, 6],
                [3, 5, 7],
                [4, 9, 2],
                [8, 5, 2],
                [6, 5, 4],
                [6, 7, 2],
                [1, 5, 9],
                [8, 3, 4]
            ];
            for (i = 0; i < rows.length; i += 1) {
                row_won = true;
                for (j = 0; j < rows[i].length; j += 1) {
                    if (this.taken[rows[i][j]] !== for_piece) {
                        row_won = false;
                        break;
                    }
                }
                if (row_won) { return true; }
            }
            return false;
        };

        this.make_move = function (cell_id) {
            var for_piece, cell = parseInt(cell_id, 0);
            for_piece = this.whos_turn();
            this.taken[cell] = for_piece;
            this.history.push(cell);
        };

        this.get_open_moves = function () {
            var cell, open_moves = [];
            for (cell in this.taken) {
                if (this.taken.hasOwnProperty(cell)) {
                    if (!this.taken[cell]) {
                        open_moves.push(cell);
                    }
                }
            }
            return open_moves;
        };
    }
    return Board;
});
