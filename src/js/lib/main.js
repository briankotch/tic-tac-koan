define(['jquery', 'ai', 'board', 'jqueryui'], function ($, AI, Board) {
    "use strict";
    function App() {
        //register this in the object so all call backs can access
        var instance = this;
        this.board = null;
        this.game_over = false;
        //click to update the state and the ui
        this.go_first_handler = function () {
            $("#go_first").toggle();
            $("#go_second").toggle();
            instance.player_goes_first = !instance.player_goes_first;
        };
        //click handler to start the game
        this.start_handler = function () {
            $('#start').toggle();
            $('#start_over').toggle();
            if (!instance.player_goes_first) {
                instance.compute_move();
            }
        };

        //click handler for when the player makes a move
        this.cell_handler = function () {
            var $cell = $(this);
            if ($cell.hasClass("available") && !instance.game_over) {
                instance.update_game(this.id);
                instance.delayed_move();
            }
        };

        //Set a few second delays for better interaction
        this.delayed_move = function () {
            setTimeout(instance.compute_move, 300);
        };

        //invoke the ai to figure out the next move
        this.compute_move = function () {
            var ai, next_move;
            ai = new AI();
            next_move = ai.run_minimax(instance.board, instance.board.whos_turn());
            console.log(next_move);
            instance.update_game(next_move);
        };

        //Update the ui
        this.log = function (player, move) {
            $("#history").append("<p>" + player + ": " + move + "</p>");
        };

        //Draw the moves
        this.update_game = function (cell_id) {
            var piece, $cell = $("#" + cell_id);
            piece = instance.board.whos_turn();
            $cell.text(piece);
            $cell.removeClass('available');
            //update the player's move
            instance.board.make_move(cell_id);
            instance.log(piece, cell_id);
            if (instance.board.is_won(piece)) {
                $("#win_dialog").dialog("open");
                instance.game_over = true;
                return;
            }
            if (instance.board.get_open_moves().length  === 0) {
                $("#draw_dialog").dialog("open");
                instance.game_over = true;
                return;
            }
        };
        //Reset the board state and player objects
        this.reset = function () {
            instance.board = new Board();
            $(".cell").each(function () {
                $(this).text("").addClass('available');
            });
            if (instance.player_goes_first) {
                instance.compute_move();
            }
            instance.game_over = false;
            $("#history").empty();
            $("#win_dialog").dialog("close");
            $("#draw_dialog").dialog("close");
        };
    }
    return App;
});
