define(["board"], function () {
    'use strict';
    function AI() {
        this.run_minimax = function (board, piece) {
            var result;
            result = this.minimax(board, piece);
            console.log(result);
            return result.move;
        };

        this.minimax = function (orig_board, for_piece) {
            var test_board, i, move, current_best_move = 0, player = "O", opponents_best_result, current_best_result = "O", board = orig_board.clone(), open_moves = board.get_open_moves();
            //initialize to the worst possible move
            if (for_piece === "O") {
                current_best_result = "X";
                player = "X";
            }
            for (i = 0; i < open_moves.length; i += 1) {
                test_board = board.clone();
                move = open_moves[i];
                test_board.make_move(move);
                if (test_board.is_won(for_piece)) {
                    return { "winner": for_piece, "move": move, "board": test_board };
                }
            }
            if (open_moves.length === 9) {
                test_board = board.clone();
                test_board.make_move(5);
                return { "winner": for_piece, "move": 5, "board": test_board };
            }
            if (open_moves.length === 1) {
                test_board = board.clone();
                test_board.make_move(open_moves[0]);
                return { "winner": "", "move": open_moves[0], "board": test_board };
            }
            for (i = 0; i < open_moves.length; i += 1) {
                test_board = board.clone();
                move = open_moves[i];
                test_board.make_move(move);
                if (test_board.is_won(for_piece)) {
                    return { "winner": for_piece, "move": move, "board": test_board };
                }
                opponents_best_result = this.minimax(test_board, player);
                if (current_best_move === 0) {
                    current_best_move = move;
                }
                if (opponents_best_result.winner === for_piece) {
                    return { "winner": for_piece, "move": move, "board": opponents_best_result.board };
                }
                if (opponents_best_result.winner === "" && current_best_result === player) {
                    current_best_move = move;
                    current_best_result = "";
                }
            }
            return { "winner": current_best_result, "move": current_best_move, "board": test_board };
        };
    }
    return AI;
});
