requirejs.config({
    baseUrl:"js/lib",
    paths: { 
        jquery: "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min",
        jqueryui: "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min"
    },
});
var app;
require(['main', 'simulator'], function(App)  {
    app = new App();
    //setup the ui and go
    $(document).ready(function () {
        $("#go_first").on("click", app.go_first_handler);
        $("#go_second").on("click", app.go_first_handler);
        $("#start").on("click", app.start_handler);
        $("#start_over").on("click", app.start_over_handler);
        $(".startover").on("click", app.reset);
        $(".cell").on("click", app.cell_handler);
        $("#win_dialog").dialog({
            dialogClass: "ttk-dialog",
            autoOpen: false
        });
        $("#draw_dialog").dialog({
            dialogClass: "ttk-dialog",
            autoOpen: false
        });
        app.reset();
    });
});
