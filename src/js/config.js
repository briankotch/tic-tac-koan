
window._skel_config = {
    prefix: "css/style",
    resetCSS: true,
    boxModel: "border",
    grid: { gutters: 30 },
    breakpoints: {
        wide: { range: "481-", containers: 960, grid: { gutters: 0 } },
        mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
    }
};
