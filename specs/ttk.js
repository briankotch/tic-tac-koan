"use strict";
define(["main", "ai", "board"], function(App, AI, Board) {
    var app = new App();
    app.reset();
    describe('How about Global Thermo Nuclear War?', function () {
        it('Both players minimaxing, should be a draw every time', function() {
            var board = new Board();
            var ai = new AI();
            for(var starting_move = 1; starting_move <= 9; starting_move += 1) {
                var test_board = board.clone();
                test_board.make_move(starting_move);
                var won = false;
                while (!won) {
                    var move = ai.run_minimax(test_board, test_board.whos_turn());
                    test_board.make_move(move);
                    if (test_board.get_open_moves().length === 0) { won = true; }
                    if (test_board.is_won("X")) { won = true; }
                    if (test_board.is_won("O")) { won = true; }
                }
                expect(test_board.get_open_moves().length).toEqual(0);
                expect(test_board.is_won("X")).toEqual(false);
                expect(test_board.is_won("O")).toEqual(false);
                draw_board(test_board);
            }
        });
    });

    function draw_board(board) { 
        var cells = [8, 1, 6, 3, 5, 7, 4, 9, 2];
        var output = "";
        while(cells.length > 0) {
            var cell = cells.shift();
            output += cell + ": " + board.taken[cell] + " ";
            if (cells.length % 3 === 0) { 
                console.log(output);
                output = "";
            }
        }
    };
});
