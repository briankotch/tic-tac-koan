'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            ttk: { 
                files:[
                    'src/js/lib/board.js',
                    'src/js/lib/player.js',
                    'src/js/lib/simulator.js',
                    'src/js/lib/main.js',
                    'src/js/lib/ai.js',
                    'src/js/lib/config.js',
                    'src/js/app.js',
                    'src/index.html',
                    'src/_layouts/default.html',
                    'src/_scss/style.scss',
                    'src/_scss/style-wide.scss',
                    'src/_scss/style-mobile.scss'
                ],
                tasks: ['test', 'build']
            }
        },
        jslint: {
            ttk: { 
                src: [
                    'src/js/lib/simulator.js', 
                    'src/js/lib/main.js', 
                    'src/js/lib/board.js',
                    'src/js/lib/player.js', 
                    'src/js/lib/ai.js'],
                
                directives: { 
                    browser: true,
                    bitwise: true,
                    predef: ['console','require', 'define']
                }
            }
        },
        sass: { 
            ttk: { 
                files: {  
                    'src/css/clear.css' : 'src/_scss/clear.scss',
                    'src/css/style.css' : 'src/_scss/style.scss',
                    'src/css/style-wide.css' : 'src/_scss/style-wide.scss',
                    'src/css/style-mobile.css' : 'src/_scss/style-mobile.scss'
                }
            }
        },
        copyto: {
            ttk: {
                files: [{
                        cwd: 'src/_site', 
                        src: ['**/*'], 
                        dest: '/Library/WebServer/Documents/ttk/'
                }]
            }
        },
        jekyll: {
            ttk: {
                options: { 
                    src: 'src/',
                    watch: false,
                    serve: false,
                    dest: 'src/_site/'
                },
            },
        },
        jasmine: { 
            src: 'src/js/lib/**/*.js',
            options : {
                specs: 'specs/*.js',
                keepRunner: true,
                template: require('grunt-template-jasmine-requirejs'),
                templateOptions: {
                    requireConfig: {
                        baseUrl: "src/js/lib",
                    }
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-jekyll');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-copy-to');
    grunt.registerTask('test', ['jslint']);
    grunt.registerTask('build', ['test', 'sass', 'jekyll', 'copyto']);
    grunt.registerTask('default', ['test', 'build', 'watch']);
};
